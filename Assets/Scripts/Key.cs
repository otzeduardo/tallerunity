﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour {
    [SerializeField]
    private Sprite[] sprites; 
    [SerializeField, Range(0, 2)]
    public int key;

	void Start () {
        SpriteRendererChange();
	}
	
	void Update () {
		
	}

    private void OnDrawGizmos()
    {
        if(!Application.isPlaying)
            SpriteRendererChange();
    }

    private void SpriteRendererChange()
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        switch (key)
        {
            case 0:
                spriteRenderer.sprite = sprites[0];
                break;
            case 1:
                spriteRenderer.sprite = sprites[1];
                break;
            case 2:
                spriteRenderer.sprite = sprites[2];
                break;
            default:
                spriteRenderer.sprite = sprites[0];
                break;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject player = collision.gameObject;
        if (player.CompareTag("Player"))
        {
            CircleCollider2D collider2D = GetComponent<CircleCollider2D>();
            SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
            int nKeys = 0;
            spriteRenderer.sprite = sprites[2];
            Destroy(collider2D);

            switch (key)
            {
                case 0:
                    nKeys = PlayerPrefs.GetInt("KeyRed");
                    nKeys++;
                    PlayerPrefs.SetInt("KeyRed", nKeys);
                    break;
                case 1:
                    nKeys = PlayerPrefs.GetInt("KeyGreen");
                    nKeys++;
                    PlayerPrefs.SetInt("KeyGreen", nKeys);
                    break;
            }
        }
    }
}
