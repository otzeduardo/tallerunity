﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Enemy : MonoBehaviour {
    [SerializeField]
    private float speed;
    [SerializeField]
    private float distance = 10f;
    [SerializeField]
    private Color color;
    private GameObject player;
    private bool playerIsDead = false;
    private Vector2 initPos;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        initPos = transform.position;
    }

    private void FixedUpdate()
    {
        transform.Rotate(Vector3.forward * Time.deltaTime * 50f);
        float dist = Vector2.Distance(player.transform.position, transform.position);
        if (dist <= distance && playerIsDead == false)
        {
            Vector2 enemyPos = Vector2.Lerp(player.transform.position, transform.position, speed * Time.deltaTime);
            transform.position = enemyPos;
        }
        else
        {
            Vector2 enemyPos = Vector2.Lerp(initPos, transform.position, 45f * Time.deltaTime);
            transform.position = enemyPos;
        }
    }

    public void OnDrawGizmos()
    {
        Handles.color = color;
        Handles.DrawSolidArc(transform.position, Vector3.forward, transform.position, 360f, distance);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(playerIsDead);
        GameObject other = collision.gameObject;
        if (other.CompareTag("Player"))
        {
            playerIsDead = true;
            
        }
    }



}
