﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {
    [SerializeField]
    private float speed = 0;
    [SerializeField]
    private float jump;
    [SerializeField]
    private Transform groundCheck;
    [SerializeField]
    private Transform cam;
    [SerializeField]
    private float smooth = 0.5f;
    private bool climbing = false;
    private Rigidbody2D rig2D;
    private Animator animator;

	void Start () {
        rig2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
		
	}
	
	void Update () {
        Vector3 pPos = new Vector3(transform.position.x, transform.position.y, transform.position.z - 10);
        Vector3 follow = Vector3.Lerp(cam.position, pPos, smooth * Time.deltaTime);
        cam.position = follow;
	}

    private void FixedUpdate()
    {
        bool isGround = Physics2D.Linecast(transform.position, groundCheck.position, 1 << 8);
        float horizontal = Input.GetAxis("Horizontal");
        Vector2 move = new Vector2(horizontal, 0);
        animator.SetFloat("Walk", Mathf.Abs(horizontal));

        rig2D.transform.Translate(move * speed);

        if(Input.GetKeyDown(KeyCode.Space) && isGround){
            rig2D.AddForce(Vector2.up * jump, ForceMode2D.Impulse);
        }

        if(climbing){
            float vertical = Input.GetAxis("Vertical");
            move = new Vector2(horizontal, vertical);
            rig2D.transform.Translate(move * speed);
        }

        Accelerate(horizontal);

    }

    private void Accelerate(float horizontal){
        if(speed != 0){
            if(speed < 0.2f){
                speed += Time.deltaTime * 0.7f;
            }else{
                speed = 0.1f;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Trigger");
        if (collision.gameObject.CompareTag("Stairs"))
        {
            climbing = true;
            rig2D.gravityScale = 0;
            rig2D.velocity = new Vector2(0, 0);
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Stairs"))
        {
            climbing = false;
            rig2D.gravityScale = 1;
        }
    }

}
