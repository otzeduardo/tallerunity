﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockKey : MonoBehaviour {
    [SerializeField]
    private Sprite[] sprites;
    [SerializeField, Range(0, 1)]
    private int block;

    private void OnDrawGizmos()
    {
        if (!Application.isPlaying)
            SpriteRendererChange();
    }

    private void SpriteRendererChange()
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        switch (block)
        {
            case 0:
                spriteRenderer.sprite = sprites[0];
                break;
            case 1:
                spriteRenderer.sprite = sprites[1];
                break;
            case 2:
                spriteRenderer.sprite = sprites[2];
                break;
            default:
                spriteRenderer.sprite = sprites[0];
                break;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject player = collision.gameObject;
        int nKeys = 0;
        if (player.CompareTag("Player"))
        {
            switch (block)
            {
                case 0:
                    RemoveKey("KeyGreen");
                    break;
                case 1:
                    RemoveKey("KeyRed");
                    break;
            }
        }
    }

    private void RemoveKey(string keyColor)
    {
        int nKeys = PlayerPrefs.GetInt(keyColor);
        if(nKeys > 0)
        {
            nKeys--;
            PlayerPrefs.SetInt(keyColor, nKeys);
            Destroy(gameObject);
        }
    }
}
